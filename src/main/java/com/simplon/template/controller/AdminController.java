package com.simplon.template.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AdminController {
    
    @GetMapping("/admin")
    public String showAdminPage() {
        return "admin/index";
    }

    @GetMapping("/admin/users")
    public String showUsersPage() {
        return "admin/users/users";
    }
}
